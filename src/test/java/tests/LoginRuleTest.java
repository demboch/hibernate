package tests;

import org.junit.Assert;
import org.junit.Test;

import check.CheckResult;
import check.RuleResult;
import check.rule.LoginRule;
import hibernate.User;

public class LoginRuleTest {

	LoginRule rule = new LoginRule();
	User user = new User();
	
	@Test
	public void should_return_error_for_login_equal_to_null() {
		user.setLogin(null);
		CheckResult actual = rule.checkRule(user);

		Assert.assertEquals(RuleResult.Error, actual.getResult());
	}
	
	@Test
	public void should_return_error_for_login_equal_to_password(){
		user.setPassword("login123");
		user.setLogin("login123");

		CheckResult actual =rule.checkRule(user);
		Assert.assertEquals(RuleResult.Error, actual.getResult());
	}

	@Test
	public void should_return_error_for_login_6_chars_or_longer(){
		user.setLogin("login");

		CheckResult actual =rule.checkRule(user);
		Assert.assertEquals(RuleResult.Error, actual.getResult());
	}
	
	@Test
	public void should_return_ok_for_correct_login() {
		user.setLogin("login1");
		CheckResult actual = rule.checkRule(user);

		Assert.assertEquals(RuleResult.Ok, actual.getResult());
	}
}
