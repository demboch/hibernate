package tests;

import org.junit.Assert;
import org.junit.Test;

import check.CheckResult;
import check.RuleResult;
import check.rule.NipRule;
import hibernate.Person;

public class NipRuleTest {

	NipRule rule = new NipRule();
	Person person = new Person();
	
	@Test
	public void should_return_error_for_nip_equal_to_null() {
		person.setNip(null);
		CheckResult actual = rule.checkRule(person);

		Assert.assertEquals(RuleResult.Error, actual.getResult());
	}

	@Test
	public void should_return_error_for_nip_with_incorrect_length() {
		person.setNip("12345678912");
		CheckResult actual = rule.checkRule(person);

		Assert.assertEquals(RuleResult.Error, actual.getResult());
	}

	@Test
	public void should_return_error_for_nip_with_letters() {
		String nip = "abcdefghij";
		person.setNip(nip);
		CheckResult actual = rule.checkRule(person);

		Assert.assertEquals(RuleResult.Error, actual.getResult());
	}

	@Test
	public void should_return_error_for_nip_with_incorrect_checksum() {
		person.setNip("1234563217");
		CheckResult actual = rule.checkRule(person);

		Assert.assertEquals(RuleResult.Error, actual.getResult());
	}

	@Test
	public void should_return_ok_for_correct_nip() {
		person.setNip("7781029219");
		CheckResult actual = rule.checkRule(person);

		Assert.assertEquals(RuleResult.Ok, actual.getResult());
	}
}
