package tests;

import java.util.Date;
import java.util.Calendar;
import org.junit.Assert;
import org.junit.Test;

import check.CheckResult;
import check.RuleResult;
import check.rule.PeselRule;
import hibernate.Person;

public class PeselRuleTest {

	PeselRule rule = new PeselRule();
	Person person = new Person();

	@Test
	public void should_return_error_for_pesel_equal_to_null() {
		person.setPesel(null);
		CheckResult actual = rule.checkRule(person);

		Assert.assertEquals(RuleResult.Error, actual.getResult());
	}

	@Test
	public void should_return_error_for_pesel_with_incorrect_length() {
		person.setPesel("9212200527");
		CheckResult actual = rule.checkRule(person);

		Assert.assertEquals(RuleResult.Error, actual.getResult());
	}

	@Test
	public void should_return_error_for_pesel_with_letters() {
		String pesel = "abcdefghijk";
		person.setPesel(pesel);
		CheckResult actual = rule.checkRule(person);

		Assert.assertEquals(RuleResult.Error, actual.getResult());
	}

	@Test
	public void should_return_error_for_pesel_with_incorrect_checksum() {
		person.setPesel("92122005271");
		CheckResult actual = rule.checkRule(person);

		Assert.assertEquals(RuleResult.Error, actual.getResult());
	}

	@Test
	public void should_return_ok_for_correct_pesel() { 
		person.setPesel("92122005270");
		CheckResult actual = rule.checkRule(person);

		Assert.assertEquals(RuleResult.Ok, actual.getResult());
	}

	@Test
	public void should_return_ok_for_pesel_with_correct_date() {
		Date dateOfBirth = new Date();

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 1992);
		cal.set(Calendar.MONTH, 12); // 0 - styczeń 
		cal.set(Calendar.DAY_OF_MONTH, 20);
		dateOfBirth = cal.getTime();

		person.setPesel("92122005270");
		person.setDateOfBirth(dateOfBirth);

		CheckResult actual = rule.checkRule(person);

		Assert.assertEquals(RuleResult.Ok, actual.getResult());
	}
}
