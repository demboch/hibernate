package tests;

import org.junit.Assert;
import org.junit.Test;

import check.CheckResult;
import check.RuleResult;
import check.rule.EmailRule;
import hibernate.Person;

public class EmailRuleTest {

	EmailRule rule = new EmailRule();
	Person person = new Person();

	@Test
	public void should_return_error_for_email_equal_to_null() {
		person.setEmail(null);
		CheckResult actual = rule.checkRule(person);

		Assert.assertEquals(RuleResult.Error, actual.getResult());
	}

	@Test
	public void should_return_ok_for_correct_email() {
		person.setEmail("test@test.test");
		CheckResult actual = rule.checkRule(person);

		Assert.assertEquals(RuleResult.Ok, actual.getResult());
	}

	@Test
	public void should_return_ok_if_email_is_valid() {
		person.setEmail("test@test.test");
		CheckResult actual = rule.checkRule(person);

		Assert.assertEquals(RuleResult.Ok, actual.getResult());
	}

}
