package tests;

import org.junit.Assert;
import org.junit.Test;

import check.CheckResult;
import check.RuleResult;
import check.rule.PasswordRule;
import hibernate.User;

public class PasswordRuleTest {

	PasswordRule rule = new PasswordRule();
	User user = new User();
	
	@Test
	public void should_return_error_for_password_equal_to_null() {
		user.setPassword(null);
		CheckResult actual = rule.checkRule(user);

		Assert.assertEquals(RuleResult.Error, actual.getResult());
	}
	
	@Test
	public void should_return_error_for_password_equal_to_login(){
		user.setLogin("login123");
		user.setPassword("login123");

		CheckResult actual =rule.checkRule(user);
		Assert.assertEquals(RuleResult.Error, actual.getResult());
	}

	@Test
	public void should_return_error_for_password_8_chars_or_longer(){
		user.setPassword("haslo");

		CheckResult actual =rule.checkRule(user);
		Assert.assertEquals(RuleResult.Error, actual.getResult());
	}
	
	@Test
	public void should_return_ok_for_correct_password() {
		user.setPassword("haslo123");
		CheckResult actual = rule.checkRule(user);

		Assert.assertEquals(RuleResult.Ok, actual.getResult());
	}
}
