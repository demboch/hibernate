package hibernate;

public class RolesPermissions extends Entity {

	private int roleId;
	private int permissionId;

	public RolesPermissions(int id, EntityState state, int permissionID, int roleID) {
		super(id, state);
		this.permissionId = permissionID;
		this.roleId = roleID;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(int permissionId) {
		this.permissionId = permissionId;
	}
}