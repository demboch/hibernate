package hibernate;

import java.util.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import repository.BaseRepository;
import unitofwork.UnitOfWork;

public class App {
	
	private static SessionFactory sessionFactory;

	public static void main(String[] args) {
		
		String url = "jdbc:mysql://localhost/hibernate";

		User user = new User();
		user.setLogin("root");
		user.setPassword("root");

		try {
			Class.forName("org.mysql.jdbcDriver");
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		try {
			Connection connection = DriverManager.getConnection(url);
			UnitOfWork unitOfWork = new UnitOfWork(sessionFactory);
			BaseRepository baseRepository = new BaseRepository(connection,unitOfWork);

			baseRepository.save(user);
			unitOfWork.saveChanges();

			List<User> usersFromDatabase = baseRepository.getAll();
			for (User users : usersFromDatabase) {
				System.out.println(users.getId() + " " + users.getLogin() + " " + users.getPassword());
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
