package hibernate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Entity;

@Entity
@Table(name="USER")
public class User extends hibernate.Entity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name="LOGIN") private String login;
	@Column(name="PASSWORD") private String password;

	@Transient
	private List<UserRoles> roles;
	@Transient
	private List<RolesPermissions> permissions;
	
	@OneToOne(mappedBy="USER")
	private Person person;

	public User() {
		roles = new ArrayList<UserRoles>();
		permissions = new ArrayList<RolesPermissions>();
	}

	public List<RolesPermissions> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<RolesPermissions> permissions) {
		this.permissions = permissions;
	}

	public List<UserRoles> getRoles() {
		return roles;
	}

	public void setRoles(List<UserRoles> roles) {
		this.roles = roles;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
		if (!this.equals(person.getUser()))
			person.setUser(this);
	}
}