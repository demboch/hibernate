package hibernate;

public enum EntityState {

	New, Modified, UnChanged, Deleted, Unknown;
}
