package check;

import hibernate.Entity;

public interface CheckRule<TEntity extends Entity> {

	public CheckResult checkRule(TEntity entity);
}
