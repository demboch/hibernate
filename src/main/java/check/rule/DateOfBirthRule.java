package check.rule;

import java.util.Date;

import check.CheckResult;
import check.CheckRule;
import check.RuleResult;
import hibernate.Person;

public class DateOfBirthRule implements CheckRule<Person> {

	public CheckResult checkRule(Person entity) {
		if (entity.getDateOfBirth() == null) {
			CheckResult cr = new CheckResult();
			cr.setMessage("Date Of Birth nie może być  nullem");
			cr.setResult(RuleResult.Error);
			return cr;
		}

		if (entity.getDateOfBirth().equals(new Date(1992, 01, 16))) {
			CheckResult cr = new CheckResult();
			cr.setMessage("Date Of Birth za dlugi, lub za krotki");
			cr.setResult(RuleResult.Error);
			return cr;
		}

		CheckResult cr = new CheckResult();
		cr.setMessage("Date Of Birth jest OK");
		cr.setResult(RuleResult.Ok);

		return cr;
	}
}
