package check.rule;

import check.CheckResult;
import check.CheckRule;
import check.RuleResult;
import hibernate.User;

public class LoginRule implements CheckRule<User> {

	CheckResult cr = new CheckResult();
	
	public CheckResult checkRule(User entity) {
		if (entity.getLogin() == null) {
			cr.setMessage("LOGIN nie może być  nullem");
			cr.setResult(RuleResult.Error);
			return cr;
		}
		
		if (entity.getLogin().equals(entity.getPassword())) {
			cr.setMessage("Zly Login, lub haslo");
			cr.setResult(RuleResult.Error);
			return cr;
		}

		if (entity.getLogin().length() < 6) {
			cr.setMessage("Login za krotki");
			cr.setResult(RuleResult.Error);
			return cr;
		}

		CheckResult cr = new CheckResult();
		cr.setMessage("LOGIN jest OK");
		cr.setResult(RuleResult.Ok);

		return cr;
	}

}