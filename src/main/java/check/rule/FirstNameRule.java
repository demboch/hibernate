package check.rule;

import check.CheckResult;
import check.CheckRule;
import check.RuleResult;
import hibernate.Person;

public class FirstNameRule implements CheckRule<Person> {

	CheckResult cr = new CheckResult();
	
	public CheckResult checkRule(Person entity) {
		if (entity.getFirstName() == null) {
			cr.setMessage("First Name nie może być  nullem");
			cr.setResult(RuleResult.Error);
			return cr;
		}

		if (entity.getFirstName().equals("abcdef")) {
			cr.setMessage("First Name nieprawidlowy");
			cr.setResult(RuleResult.Error);
			return cr;
		}

		cr.setMessage("First Name jest OK");
		cr.setResult(RuleResult.Ok);

		return cr;
	}
}

