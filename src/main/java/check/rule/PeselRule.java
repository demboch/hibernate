package check.rule;

import java.text.SimpleDateFormat;

import check.CheckResult;
import check.CheckRule;
import check.RuleResult;
import hibernate.Person;

public class PeselRule implements CheckRule<Person> {

	CheckResult cr = new CheckResult();

	public CheckResult checkRule(Person entity) {
		if (entity.getPesel() == null) {
			cr.setMessage("PESEL nie może być  nullem");
			cr.setResult(RuleResult.Error);
			return cr;
		}

		if (entity.getPesel().length() != 11) {
			cr.setMessage("PESEL za dlugi, lub za krotki");
			cr.setResult(RuleResult.Error);
			return cr;
		}

		char[] peselArray = entity.getPesel().toCharArray();
		for (int i = 0; i < peselArray.length; i++) {
			if (!(peselArray[i] >= 48 && peselArray[i] <= 57)) {
				cr.setMessage("PESEL nie moze zawierac liter");
				cr.setResult(RuleResult.Error);
				return cr;
			}
		}

		int[] weights = { 1, 3, 7, 9, 1, 3, 7, 9, 1, 3 };
		int sum = 0;
		for (int i = 0; i < 10; i++) {
			sum += Character.getNumericValue(peselArray[i]) * weights[i];
		}
		
		sum = 10 - (sum % 10);
		if(sum == 10) sum = 0;

		if (sum != Character.getNumericValue(peselArray[10])) {
			cr.setMessage("Nie prawidlowa suma kontrolna");
			cr.setResult(RuleResult.Error);
			return cr;
		}

		cr.setMessage("PESEL jest OK");
		cr.setResult(RuleResult.Ok);

		return cr;
	}

	public static boolean isPeselAndDateSame(Person entity) {
		SimpleDateFormat date = new SimpleDateFormat("yyyyMMdd");
		StringBuilder str = new StringBuilder();

		str.append(getBirthYear(entity.getPesel())).append(getBirthMonth(entity.getPesel()))
				.append(getBirthDay(entity.getPesel()));

		if (!str.toString().equals(date.format(entity.getDateOfBirth())))
			return false;
		return true;
	}

	public static int getBirthYear(String pesel) {
		int year, month;

		year = 10 * Character.getNumericValue(pesel.charAt(0));
		year += Character.getNumericValue(pesel.charAt(1));

		month = 10 * Character.getNumericValue(pesel.charAt(2));
		month += Character.getNumericValue(pesel.charAt(3));

		if (month > 80 && month < 93) {
			year += 1800;
		} else if (month > 0 && month < 13) {
			year += 1900;
		} else if (month > 20 && month < 33) {
			year += 2000;
		} else if (month > 40 && month < 53) {
			year += 2100;
		} else if (month > 60 && month < 73) {
			year += 2200;
		}
		return year;
	}

	public static int getBirthMonth(String pesel) {
		int month;
		month = 10 * Character.getNumericValue(pesel.charAt(2));
		month += Character.getNumericValue(pesel.charAt(3));
		if (month > 80 && month < 93) {
			month -= 80;
		} else if (month > 20 && month < 33) {
			month -= 20;
		} else if (month > 40 && month < 53) {
			month -= 40;
		} else if (month > 60 && month < 73) {
			month -= 60;
		}
		return month;
	}

	public static int getBirthDay(String pesel) {
		int day;
		day = 10 * Character.getNumericValue(pesel.charAt(4));
		day += Character.getNumericValue(pesel.charAt(5));
		return day;
	}

}
