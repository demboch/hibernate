package check.rule;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import check.CheckResult;
import check.CheckRule;
import check.RuleResult;
import hibernate.Person;

public class EmailRule implements CheckRule<Person> {

	CheckResult cr = new CheckResult();
	
	public CheckResult checkRule(Person entity) {
		if (entity.getEmail() == null) {
			cr.setMessage("EMAIL nie może być  nullem");
			cr.setResult(RuleResult.Error);
			return cr;
		}

		cr.setMessage("EMAIL jest OK");
		cr.setResult(RuleResult.Ok);

		return cr;
	}

	public boolean isEmailValid(String email) {

		boolean result;
		Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
		Matcher m = p.matcher(email);

		boolean matchFound = m.matches();

		if (matchFound)
			result = true;
		else
			result = false;

		return result;
	}
}
