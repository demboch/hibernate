package check.rule;

import check.CheckResult;
import check.CheckRule;
import check.RuleResult;
import hibernate.User;

public class PasswordRule implements CheckRule<User> {

	CheckResult cr = new CheckResult();

	public CheckResult checkRule(User entity) {
		if (entity.getPassword() == null) {
			cr.setMessage("HASLO nie może być  nullem");
			cr.setResult(RuleResult.Error);
			return cr;
		}

		if (entity.getPassword().equals(entity.getLogin())) {
			cr.setMessage("Zly Login, lub haslo");
			cr.setResult(RuleResult.Error);
			return cr;
		}

		if (entity.getPassword().length() < 8) {
			cr.setMessage("HASLO za krotkie");
			cr.setResult(RuleResult.Error);
			return cr;
		}

		cr.setMessage("HASLO jest OK");
		cr.setResult(RuleResult.Ok);

		return cr;
	}

}