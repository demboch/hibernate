package unitofwork;

import hibernate.*;
import repository.*;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import cache.Cache;
import cache.cachedrepository.AddressCachedRepository;
import cache.cachedrepository.EnumerationValueCachedRepository;
import cache.cachedrepository.PersonCachedRepository;
import cache.cachedrepository.PhoneNumberCachedRepository;
import cache.cachedrepository.UserCachedRepository;
import cachedrepository.*;

public class UnitOfWork {
	
	private IRepository<Person> people;
	private IRepository<User> user;
	private IRepository<Address> address;
	private IRepository<PhoneNumber> phoneNumber;
	private IRepository<EnumerationValue> enumerationValue;
	
	private SessionFactory sessionFactory;
	private Session session;
	
	public UnitOfWork(SessionFactory sessionFactory){
		this.sessionFactory = sessionFactory;
		this.session = sessionFactory.getCurrentSession();
		//this.people = new PersonRepository(session);
		this.people = new PersonCachedRepository(session, Cache.getInstance().getPersons());
		this.user = new UserCachedRepository(session, Cache.getInstance().getUser());
		this.address = new AddressCachedRepository(session, Cache.getInstance().getAddress());
		this.phoneNumber = new PhoneNumberCachedRepository(session, Cache.getInstance().getPhoneNumber());
		this.enumerationValue = new EnumerationValueCachedRepository(session, Cache.getInstance().getEnumerationValue());	
	}
	
	public IRepository<Person> getPeople(){
		return people;
	}
	
	public IRepository<User> getUser() {
		return user;
	}

	public IRepository<Address> getAddress() {
		return address;
	}

	public IRepository<PhoneNumber> getPhoneNumber() {
		return phoneNumber;
	}

	public IRepository<EnumerationValue> getEnumerationValue() {
		return enumerationValue;
	}

	public void saveChanges(){
		session.flush();
		session.close();
	}
}