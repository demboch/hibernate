package unitofwork;

import hibernate.Entity;

public interface IUnitOfWorkRepository {

    void persistAdd(Entity entity);
    void persistDelete(Entity entity);
    void persistUpdate(Entity entity);
}