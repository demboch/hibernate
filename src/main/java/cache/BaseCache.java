package cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BaseCache<T> implements ICache<T> {

	private Class<T> entityClass;
	private Map<Integer, T> cacheItems;
	private boolean isValid;

	public BaseCache(Class<T> entityClass) {
		this.cacheItems = new HashMap<Integer, T>();
		this.entityClass = entityClass;
		this.isValid = false;
	}

	public List<T> getAll() {
		List<T> result = new ArrayList<T>();
		result.addAll(cacheItems.values());
		return result;
	}

	public T getById(int id) {
		return cacheItems.get(id);
	}

	public boolean isValid() {
		return isValid;
	}

	public void set(Map<Integer, T> list) {
		this.cacheItems.putAll(list);
		this.isValid = true;
	}

	public void invalidate() {
		this.isValid = false;
	}
}
