package cache;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import repository.IRepository;

public abstract class BaseCachedRepository<T> implements IRepository<T> {

	private Session session;
	private Class<T> entityClass;
	private ICache<T> cache;
	
	protected BaseCachedRepository(Session session, Class<T> entityClass, ICache<T> cache){
		this.session = session;
		this.entityClass = entityClass;
		this.cache = cache;
	}
	
	@Override
	public List<T> getAll() {
		if(!cache.isValid()) {
			updateCache();
		}
		return cache.getAll();
	}

	@Override
	public T getById(int id) {
		if(!cache.isValid()) {
			updateCache();
		}	
		return cache.getById(id);
	}
	
	private void updateCache() {
		List<T> entity = createCriteria().list();
		cache.set(getMap(entity));
	}
	
	protected abstract Map<Integer, T> getMap(List<T> entities);

	private Criteria createCriteria() {
		return session.createCriteria(entityClass);
	}

	@Override
	public void save(T entity) {
		session.saveOrUpdate(entity);
		cache.invalidate();
	}

	@Override
	public void delete(int id) {
		T entityToDelete = getById(id);
		session.delete(entityToDelete);
		cache.invalidate();
	}
}
