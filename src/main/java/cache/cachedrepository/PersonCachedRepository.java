package cache.cachedrepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import cache.BaseCachedRepository;
import cache.ICache;
import hibernate.Person;

public class PersonCachedRepository extends BaseCachedRepository<Person> {

	public PersonCachedRepository(Session session, ICache<Person> cache) {
		super(session, Person.class, cache);
	}

	@Override
	protected Map<Integer, Person> getMap(List<Person> entities) {
		Map<Integer, Person> map = new HashMap<Integer, Person>();
		for (Person person : entities) {
			map.put(person.getId(), person);
		}
		return map;
	}

}
