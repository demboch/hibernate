package cache.cachedrepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import cache.BaseCachedRepository;
import cache.ICache;
import hibernate.User;

public class UserCachedRepository extends BaseCachedRepository<User> {

	public UserCachedRepository(Session session, ICache<User> cache) {
		super(session, User.class, cache);
	}

	@Override
	protected Map<Integer, User> getMap(List<User> entities) {
		Map<Integer, User> map = new HashMap<Integer, User>();
		for (User user : entities) {
			map.put(user.getId(), user);
		}
		return map;
	}

}
