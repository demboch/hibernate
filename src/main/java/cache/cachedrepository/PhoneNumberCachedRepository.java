package cache.cachedrepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import cache.BaseCachedRepository;
import cache.ICache;
import hibernate.PhoneNumber;

public class PhoneNumberCachedRepository extends BaseCachedRepository<PhoneNumber> {

	public PhoneNumberCachedRepository(Session session, ICache<PhoneNumber> cache) {
		super(session, PhoneNumber.class, cache);
	}

	@Override
	protected Map<Integer, PhoneNumber> getMap(List<PhoneNumber> entities) {
		Map<Integer, PhoneNumber> map = new HashMap<Integer, PhoneNumber>();
		for (PhoneNumber number : entities) {
			map.put(number.getId(), number);
		}
		return map;
	}

}
