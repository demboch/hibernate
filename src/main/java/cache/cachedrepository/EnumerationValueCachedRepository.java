package cache.cachedrepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import cache.BaseCachedRepository;
import cache.ICache;
import hibernate.EnumerationValue;

public class EnumerationValueCachedRepository extends BaseCachedRepository<EnumerationValue> {

	public EnumerationValueCachedRepository(Session session, ICache<EnumerationValue> cache) {
		super(session, EnumerationValue.class, cache);
	}

	@Override
	protected Map<Integer, EnumerationValue> getMap(List<EnumerationValue> entities) {
		Map<Integer, EnumerationValue> map = new HashMap<Integer, EnumerationValue>();
		for (EnumerationValue value : entities) {
			map.put(value.getId(), value);
		}
		return map;
	}

}
