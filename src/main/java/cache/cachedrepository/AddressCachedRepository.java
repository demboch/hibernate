package cache.cachedrepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import cache.BaseCachedRepository;
import cache.ICache;
import hibernate.Address;

public class AddressCachedRepository extends BaseCachedRepository<Address> {

	public AddressCachedRepository(Session session, ICache<Address> cache) {
		super(session, Address.class, cache);
	}

	@Override
	protected Map<Integer, Address> getMap(List<Address> entities) {
		Map<Integer, Address> map = new HashMap<Integer, Address>();
		for (Address address : entities) {
			map.put(address.getId(), address);
		}
		return map;
	}

}
