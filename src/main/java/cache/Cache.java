package cache;

import hibernate.*;

public class Cache {

	private static Cache instance;

	public static Cache getInstance() {
		if (instance == null) {
			instance = new Cache();
		}
		return instance;
	}

	
	
	private BaseCache<Person> persons;
	private BaseCache<User> user;
	private BaseCache<Address> address;
	private BaseCache<PhoneNumber> phoneNumber;
	private BaseCache<EnumerationValue> enumerationValue;

	private Cache() {
		persons = new BaseCache<Person>(Person.class);
		user = new BaseCache<User>(User.class);
		address = new BaseCache<Address>(Address.class);
		phoneNumber = new BaseCache<PhoneNumber>(PhoneNumber.class);
		enumerationValue = new BaseCache<EnumerationValue>(EnumerationValue.class);
	}

	public BaseCache<Person> getPersons() {
		return persons;
	}

	public BaseCache<User> getUser() {
		return user;
	}

	public BaseCache<Address> getAddress() {
		return address;
	}

	public BaseCache<PhoneNumber> getPhoneNumber() {
		return phoneNumber;
	}

	public BaseCache<EnumerationValue> getEnumerationValue() {
		return enumerationValue;
	}

}
