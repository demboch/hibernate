package repository;

import org.hibernate.Session;

import hibernate.PhoneNumber;

public class PhoneNumberRepository
	extends BaseRepository<PhoneNumber> 
	implements IRepository<PhoneNumber> {

	public PhoneNumberRepository(Session s) {
		super(s, PhoneNumber.class);
	}	
}
