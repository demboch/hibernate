package repository;

import org.hibernate.Session;

import hibernate.User;

public class UserRepository
	extends BaseRepository<User> 
	implements IRepository<User> {

	public UserRepository(Session s) {
		super(s, User.class);
	}
}
