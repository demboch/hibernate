package repository;

import java.util.List;

public interface IRepository<T> {

	public List<T> getAll();	
	public T getById(int id);
	public void save(T entity);
	public void delete(int id);
}
