package repository;

import org.hibernate.Session;

import hibernate.Address;

public class AddressRepository
	extends BaseRepository<Address>
	implements IRepository<Address> {

	public AddressRepository(Session s) {
		super(s, Address.class);
	}
}
