package repository;

import org.hibernate.Session;

import hibernate.Person;

public class PersonRepository 
	extends BaseRepository<Person> 
	implements IRepository<Person> {

	public PersonRepository(Session s) {
		super(s, Person.class);
	}	
}
