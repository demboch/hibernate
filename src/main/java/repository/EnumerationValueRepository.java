package repository;

import org.hibernate.Session;

import hibernate.EnumerationValue;

public class EnumerationValueRepository
	extends BaseRepository<EnumerationValue> 
	implements IRepository<EnumerationValue> {

	public EnumerationValueRepository(Session s) {
		super(s, EnumerationValue.class);
	}	
}
